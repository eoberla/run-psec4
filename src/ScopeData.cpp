//ScopeData.cpp
//ejo 2012

#include "PSEC4_EVAL.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

int main(int argc, char **argv){
  
  PSEC4_EVAL command;
  unsigned int num_frames = atoi(argv[1]);
  int trig_mode  = atoi(argv[2]);
  int scope_mode  = atoi(argv[3]);

  if(command.INITIALIZE() != 0){
    return 1;
  }
  
  if(trig_mode != 0)
    command.USB_SYNC(true);

  command.LOG_DATA_SCOPE(1, num_frames, trig_mode, scope_mode);

  return 0;

}
