//EnableTrig.cpp
//
//ejo 2012

#include "PSEC4_EVAL.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

int
main(int argc, char **argv){

  PSEC4_EVAL trigger;
  bool ENABLE = atoi(argv[1]);

  if(trigger.INITIALIZE() != 0){
    return 1;
  }

  trigger.INT_TRIG_ENABLE(ENABLE);
  usleep(10000);
  //trigger.SOFT_TRIG();
  //trigger.Read(false);

  return 0;
}
