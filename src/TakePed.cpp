//TakePed.cpp
//ejo 2012

#include "PSEC4_EVAL.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

int main(void){
  
  PSEC4_EVAL command;

  if(command.INITIALIZE() != 0){
    return 1;
  }

  command.SOFT_TRIG();
  command.Read(false);
  float ped_voltage = command.VBIAS * 1200/4096;
  //cout << ped_voltage << endl; //

  usleep(10000);

  command.GENERATE_PED(true);
  cout << "PSEC-4 ped data taken @ " << ped_voltage  << " mV\n";

  return 0;
}
