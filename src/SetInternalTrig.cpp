//SetInternalTrig.cpp
//
//ejo 2012

#include "PSEC4_EVAL.h"
#include <fstream>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <math.h>

using namespace std;

int
main(int argc, char **argv){

  PSEC4_EVAL internal_trigger;
  
  bool trig_sign = atoi(argv[1]);
  unsigned int trig_threshold = atoi(argv[2]);

  if(internal_trigger.INITIALIZE() != 0){
    return 1;
  }

  internal_trigger.INT_TRIG_RESET();

  usleep(2000);
  internal_trigger.USB_DEBUG(trig_threshold);
  internal_trigger.INT_TRIG_SETSIGN(trig_sign);
  
  usleep(100000);
  //internal_trigger.INT_TRIG_RESET();
  //
  return 0;
}

  
  
